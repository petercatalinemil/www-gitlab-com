title: Crédit Agricole
cover_image: '/images/blogimages/creditagricole-cover-image.jpg'
cover_title: |
  How Crédit Agricole Corporate & Investment Bank (CACIB) transformed its global workflow with GitLab
cover_description: |
  Through user preference, CACIB adopted GitLab for source code management (SCM) robust integration management, and seamless automation.
canonical_path: /customers/credit-agricole/
file_name: credit-agricole
twitter_image: '/images/case_study_logos/credit-agricole-logo-1.png'

twitter_text: "Learn how @CreditAgricole transformed its global workflow with GitLab."

customer_logo: '/images/case_study_logos/credit-agricole-logo-1.png'
customer_industry: Financial Services
customer_location: 37 locations (Europe, Asia-Pacific, Americas, Africa Middle-East)
customer_solution: GitLab Enterprise
customer_employees: 8,325 
customer_overview: |
  Worldwide banking institute, Crédit Agricole, adopted GitLab for SCM, but found it offered automation and improved efficiency.
customer_challenge: |
  Crédit Agricole CIB was looking to move to a Git solution and wanted users to decide on the platform.

key_benefits: >-
    
    Improved workflow efficiency 
    
    
    Cross-company collaboration
    
   
    Ease of integration
    
    
    Faster time to delivery
    
    
    Integrated task management
    
    
    Provided robust automation 

customer_stats:
  - stat: 50-75%  
    label: Time to delivery saved
  - stat: 2,000      
    label: Organic users 
  - stat: <1 day   
    label: To create a new application 


customer_study_content:
  - title: the customer
    subtitle: French banking leader
    content: >-
        
        
        Crédit Agricole CIB is the corporate and investment banking arm of Crédit Agricole Group, the twelfth largest banking group worldwide in terms of tier 1 capital (The Banker, July 2019). Nearly 8,000 employees across Europe, the Americas, Asia-Pacific, the Middle East and Africa support the Bank's clients, meeting their financial needs throughout the world. Crédit Agricole CIB offers its large corporate and institutional clients a range of products and services in capital markets activities, investment banking, structured finance, commercial banking and international trade. The corporate identity of the financial institution is called Crédit Agricole CIB, and there are teams in approximately 37 countries, including the division of information systems of Crédit Agricole CIB called Global IT (GIT).

  - title: the challenge
    subtitle:  Improving worldwide workflow efficiency
    content: >-
        
        
        [Crédit Agricole CIB]( https://www.ca-cib.com/) was using centralized source code management solutions since 2011. Over time, the need for alternate options that were able to increase performances, enhance user experience and flexibility emerged, particularly for development teams spread in different areas such as Paris, London, or Singapore.   
        
        
        On top of that, branch management wasn’t standardized, and elements that weren’t source code were being stored in repositories, which had an impact on performances and on user experience. “We primarily needed to manage our solutions globally. Everyone uses Git, whether they need a decentralized SCM or not, it is such a predominant tool in the market that it is way easier to onboard new people when using such industry standards,” said Nicolas Jautée, Software Factory and DevOps Expert.
        
        
        There was a lack of native integration with the tooling systems, and also amongst the teams. The DevOps teams manage the platform element and create access in order for teams to then manage their own content, creating as many repositories as necessary for code storage without admin constraint. One of the reasons Crédit Agricole CIB was looking for a new source code management platform was for integration with autonomy. “The goal is to have teams that can be independent and responsible within their perimeters with our support and our expertise (if needed), while being able to go as far as possible on their own to be reactive within their businesses,” Jautée said. 
   
  - title: the solution
    subtitle: GitLab spreads through word of mouth
    content: >-
        
        
        Crédit Agricole CIB intended to adopt an implementation of Git platform, which led to a comparative study and proof of concepts between various solutions. “At that time, we looked at GitLab and noticed that it offered great functionalities. In terms of implementation, it was more relevant and easier to manage than black box platforms,” Jautée said. Moreover, other entities within the Crédit Agricole group were already using GitLab with success. 
        
        
        Previously, Crédit Agricole CIB had done a couple of [SCM](/stages-devops-lifecycle/source-code-management/) top down migrations over time. However, with GitLab they did not want to force teams to migrate: “Consequently, for GitLab, we let word of mouth and user preference do the job. The fact is that the licenses we bought the first year were used within less than 9 months. I think we had to purchase more licenses even before the end of the first year even though we had not done any official communication on this tool; we only let viral adoption take its course,” Jautée said.
        
        
        Contrary to what is customary, GitLab was launched from the bottom up, promoted initially from the project teams. Infrastructure teams also helped by testing functionalities and documentation. The bottom-up building and viral distribution model resulted in a rapid adoption rate. “People who really wanted to use this tool knew why they liked it; we didn’t launch a migration campaign imposed by our top management. So far, we haven’t found it necessary to do so, since projects have been coming in naturally and users saw the benefits. This is to the point where today, we provide IT for IT, since that’s our job, but we quickly found ourselves in a situation where the tool is exceeding the initial perimeter we anticipated,” said Sedat Guclu, Head of Software Factory and Devops.
        
        
        With previous solutions, they had to take care of all the administrative steps, adding users, maintaining logins, etc. The process was hard to manage and time consuming, causing lengthy delays for those using it. One priority was to free up developer time with a self-service solution. Consequently, GitLab provided significant cost savings that they hadn’t previously planned on. “The community-based solution, with unlimited and free use, enables us to create documentation and tutorials, make POCs, prototypes and inner source development, to which we can have everyone contribute without having to worry about the number of licenses required,” Guclu said. “So, we keep a community-based platform, and we have an ‘Enterprise’ platform hosting the business code and all the automation systems, whether from infrastructure, CI-CD or other, to be able to cover both needs and better distribute the usage accordingly.” 
          
  - title: the results
    subtitle: Exceeding business application expectations
    content: >-
        
        
        “GitLab is much more than a mere source manager. There is a whole ecosystem around it: integrated task management, integrated CI, etc., which is totally absent from our previous solutions. These were the primary aspects that motivated our initial approach,” Jautée added.
        
        
        A standout benefit of GitLab is the ability to integrate it into the existing environment. It is simpler for the teams to use a tool that can be installed and mastered quickly. The second benefit that stood out over its competitors is the superior functionality. The ability to not only manage code, but also delegate the long-term management of the content to perimeter managers.    
        
        
        The infrastructure teams now use GitLab for automation tasks, including machine building on a private cloud. Application release processes are also based in GitLab. “This isn’t exactly what we had foreseen. While our goal was to host the code for our business applications, other teams, by extension, saw additional benefits,” Guclu said.
        
        
        Crédit Agricole CIB now has GitLab users on both IT and the business side, using the tool for IT tasks and guidelines/how to’s. Some team members use Mermaid or LaTeX modules that integrate into GitLab to generate scientific documentation. “These are more uses that we hadn’t predicted. The viral mode went well beyond what we originally imagined, since we started with a small number of users. In the first year we had 400 users and today we have 2,000 [Enterprise users](/enterprise/), and this number is increasing daily. The growth is now self-sustaining, and that has helped us enormously, both in terms of standardization and automation,” according to Guclu.  
        
        
        Onboarding has also been simplified and streamlined with GitLab. According to Guclu, “We might have needed five to 10 days to apply onboarding. Today, we can do it within a day. It only takes a few hours to create the space. If the person already has the authorizations, it’s almost instantaneous: We do it in minutes, then they manage the entire process and create their own repositories.”
        
        
        Developers now have the ability to perform migrations themselves with existing code. The primary benefit is that it allows them to make ‘blank’ migrations to test the retrieved code and adapt environments to be able to run in parallel with the application development. In the meantime, they can execute tests in GitLab while continuing deliveries, since a ‘hot fix’ isn’t necessary on the application. “For a migration, we went from almost a month with previous migration strategies to a few days that can be modulated according to the project priorities. It’s up to them to decide when they want to do it, how they test, and how they configure their factories at the same time, to continue delivering the existing applications while verifying that the work is done the same way in GitLab. That’s also a real comfort,” Guclu said.
        
        
        By the end of 2021, Crédit Agricole CIB plans to host all the open source code within GitLab. In the three years since the adoption of GitLab, they went from zero users to 1500 without forcing it on teams. There is now a large automation chain, with over 200 runners, which means that they not only adopted GitLab for code management, but also for automation. “There are data scientists who are also using GitLab for notebooks they develop with Anaconda or similar solutions, that never used this type of solution before. They didn’t see any advantage in managing their source code in this type of tool. That is actually what they told us,” Guclu said. “Today, we see that they are also asking for this type of solution, because it allows them to collaborate more easily with other members of their team.”
        
        
        The success of the adoption of GitLab within CACIB among other entities contributed to the promotion of this solution as a standard within Crédit Agricole group, with empowerment on inner and open source initiatives.
        


        
        
        ## Learn more about GitLab solutions
        
        
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
        
        
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
        
        
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: Switching to GitLab made us go from several days to create a new application to just a few hours. 
    attribution: Sedat Guclu 
    attribution_title: Head of Software Factory and Devops, Crédit Agricole
