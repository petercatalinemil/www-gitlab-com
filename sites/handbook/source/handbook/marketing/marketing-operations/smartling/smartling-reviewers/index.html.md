---
layout: handbook-page-toc
title: "Smartling Reviewers"
description: "This page is a dedicated resource for Smartling internal reviewers."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

## Smartling Reviewer Platform

As you log onto the platform, you will be on your jobs page. There are three important side bars on this page:

- **Current work**: Any work that is actively in review and is assigned to you. 
- **Available to Accept**: This is any work that has reach the internal review step. Here is where content will stay until someone claims the job. 
- **Upcoming**: Any work that is in the editing step of the translation workflow. It could reach the review step in the next day or two. 

Once content is claimed it will populate in Current Work. 

Under current work, reviewers can see the following detail:
- Job Name, and the project it is under (Documents, Marketo, GDN). 
- The progress bar, and the number of words in a job. 

Task due date: The due date for the particular workflow step (ie. the due date for the review to be complete). This is not set up currently so you will not see this populated in the tool. 
Job Due Date: This is the due date for the entire job to be translated.

# Review Mode

Once you click open- it will go into Review mode, which gives you an option to see this bi lingual or mono lingual. Here you will see the visual context of the content- eg if you are translating an email in Marketo , you should be able to see the email design. 

You are able to reject/ approve strings in the review made. 

If you wish to make changes to the content yourself -  then please switch to the CAT tool at the top of the page. 

# CAT Tool

Here the following tabs are available:

- **Job Briefing**: This holds Gitlab content style guide and any additional attachments to the job. 
- **Context**: If you job has come from the Marketo or GDN page, it will here show you the webpage/email/etc. so you have context to the translation. 
- **Quality Insurance**: This will show any potential errors including spellings, translation errors, spacing, etc. 
- **Translation Memory**: automatically populated. The percentages indicated whether there is memory match. You can search the translation memory for words/phrases and inserted them into translation using the “Insert TM” button.
- **Glossary** : reviewers are able to search the glossary. These should be applied by the time content has reached the review step - although the glossary term may be a “do not translate” term. 

On the right hand column in the CAT tool, there are some more information like

- **Quality assurance report**: Takes into account 100 data points to score the quality of the translation. 70 or higher is a good point of quality. You can download this report at the top of the page and slice the data as you wish. 
- **Issues**: This is a way for you to communicate directly with the linguist who translated the job and Any existing issues that have been raised will be available in this tab.

If you have an issue with a particular string, then click into the string, then on the right hand side, open a review Issue. This will open up a pop up that will allow you to define the description of issue and the following:

Issue Type: Source (Marketo, Document, GDN) OR Translation (Translation issues are comments, questions or queries in relation to the translation submitted for a source string. ) 

Once you are done, you can submit the job at the top of the page and the job will be published.

## Notifications:

As a reviewer, there are some email notifications that we recommend turning on. Please be mindful that even if these are on, the emails may go to your spam inbox. Watch this helpful video to see [how to turn these notifications on](https://drive.google.com/file/d/14tOVmc5cCrVq2x_WzArW0eytzAKWmM3c/view?usp=sharing).

The two notifications are the following:

- **Content Available for work**: If you are manually assigned any jobs- this will happen if you are the only reviewer for a language. 
- **Content Available for claim**: If jobs come into the available to accept tab, a notification will be sent. 
